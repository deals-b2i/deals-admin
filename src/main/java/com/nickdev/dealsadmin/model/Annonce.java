package com.nickdev.dealsadmin.model;



import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "annonces")
public class Annonce {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    private String title;

    @Column(nullable = false)
    private String description;

    @Column()
    private String secteur;

    @Column()
    private String client;

    @Column()
    private String localisation;

    @Column()
    private String contact;

    @Column(name = "status", nullable = false)
    private int status;

    @Column(name = "note", nullable = false)
    private int note;

    @DateTimeFormat(pattern="dd/MM/yyyy HH:mm:ss")
    private Date datePublication;


}
