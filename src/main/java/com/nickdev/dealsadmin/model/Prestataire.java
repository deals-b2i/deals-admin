package com.nickdev.dealsadmin.model;

import lombok.Data;

import javax.persistence.*;


@Data
@Entity
@Table(name = "prestataires")
public class Prestataire {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String nom;

    @Column(nullable = false)
    private String prenoms;

    @Column(nullable = false)
    private String contact;

    @Column(nullable = true)
    private String bio;

    @Column(nullable = true)
    private String piece_face_A;

    @Column(nullable = true)
    private String piece_face_B;

    @Column(nullable = false )
    private int status;

    @Column(nullable = false, unique = true, length = 45)
    private String email;

    @Column(nullable = false, length = 64)
    private String password;


}
