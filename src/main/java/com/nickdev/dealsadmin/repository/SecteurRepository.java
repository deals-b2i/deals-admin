package com.nickdev.dealsadmin.repository;

import com.nickdev.dealsadmin.model.Secteur;
import org.springframework.data.repository.CrudRepository;

public interface SecteurRepository extends CrudRepository<Secteur, Long> {
}
