package com.nickdev.dealsadmin.repository;

import com.nickdev.dealsadmin.model.Annonce;
import org.springframework.data.repository.CrudRepository;

public interface AnnonceRepository extends CrudRepository<Annonce, Long> {
}
