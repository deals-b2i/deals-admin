package com.nickdev.dealsadmin.repository;

import com.nickdev.dealsadmin.model.Prestataire;
import org.springframework.data.repository.CrudRepository;

public interface PrestataireRepository extends CrudRepository<Prestataire, Long> {
}
