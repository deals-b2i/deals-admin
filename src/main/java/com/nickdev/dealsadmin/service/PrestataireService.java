package com.nickdev.dealsadmin.service;

import com.nickdev.dealsadmin.model.Prestataire;
import com.nickdev.dealsadmin.repository.PrestataireRepository;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Data
@Service
public class PrestataireService {

    @Autowired
    private PrestataireRepository prestataireRepository;

    public Optional<Prestataire> getPrestataire(final Long id) {
        return prestataireRepository.findById(id);
    }

    public Iterable<Prestataire> getPrestataires() {
        return prestataireRepository.findAll();
    }

    public void deletePrestataire(final Long id) {
        prestataireRepository.deleteById(id);
    }

    public Prestataire savePrestataire(Prestataire prestataire) {
        Prestataire savedPrestataire = prestataireRepository.save(prestataire);
        return savedPrestataire;
    }
}
