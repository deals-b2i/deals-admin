package com.nickdev.dealsadmin.controller;

import lombok.Data;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Data
@Controller
public class DashboardController {

    @GetMapping("/")
    String index() {
        return "dashboard";
    }
}
