package com.nickdev.dealsadmin.controller;

import com.nickdev.dealsadmin.model.Prestataire;
import com.nickdev.dealsadmin.service.PrestataireService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Optional;

@Data
@Controller
public class PrestataireController {

    @Autowired
    private PrestataireService prestataireService;

    @GetMapping("/prestataires")
    public String index(Model model) {
        Iterable<Prestataire> prestataires = prestataireService.getPrestataires();
        model.addAttribute("prestataires", prestataires);

        return "prestataires/index";
    }

    @GetMapping("/prestataires/delete/{id}")
    public String delete(@PathVariable(name = "id") Long id) {
        prestataireService.deletePrestataire(id);
        return "redirect:/prestataires";
    }

    @PostMapping("/prestataires/store")
    public String store(@ModelAttribute("prestataire") Prestataire prestataire) {
        prestataireService.savePrestataire(prestataire);

        return "redirect:/prestataires";
    }

    @GetMapping("/prestataires/update/status/{id}")
    public String updateStatus(@PathVariable(name = "id") Long id) {
        Optional<Prestataire> prestataire = prestataireService.getPrestataire(id);
        if (prestataire.get().getStatus() == 0) {
            prestataire.get().setStatus(1);
        } else {
            prestataire.get().setStatus(0);
        }
        prestataireService.savePrestataire(prestataire.get());
        return "redirect:/prestataires";
    }
}
