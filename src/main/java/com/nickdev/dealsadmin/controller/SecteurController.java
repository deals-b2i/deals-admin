package com.nickdev.dealsadmin.controller;

import com.nickdev.dealsadmin.model.Secteur;
import com.nickdev.dealsadmin.service.SecteurService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Optional;


@Data
@Controller
public class SecteurController {


    @Autowired
    private SecteurService secteurService;

    @GetMapping("/secteurs")
    public String index(Model model) {
        Iterable<Secteur> secteurs = secteurService.getSecteurs();
        model.addAttribute("secteurs", secteurs);

        return "secteurs/index";
    }

    @GetMapping("/secteurs/create")
    public String create(Model model) {
        Secteur secteur = new Secteur();
        model.addAttribute("secteur", secteur);

        return "secteurs/create";
    }

    @PostMapping("/secteurs/store")
    public String store(@ModelAttribute("secteur") Secteur secteur) {
        secteurService.saveSecteur(secteur);

        return "redirect:/secteurs";
    }

    @GetMapping("/secteurs/edit/{id}")
    public ModelAndView edit(@PathVariable(name = "id") Long id) {
        ModelAndView mav = new ModelAndView("secteurs/edit");
        Optional<Secteur> secteur = secteurService.getSecteur(id);
        mav.addObject("secteur", secteur);

        return mav;
    }

    @GetMapping("/secteurs/delete/{id}")
    public String delete(@PathVariable(name = "id") Long id) {
        secteurService.deleteSecteur(id);
        return "redirect:/secteurs";
    }

    @GetMapping("/secteurs/update/status/{id}")
    public String updateStatus(@PathVariable(name = "id") Long id) {
        Optional<Secteur> secteur = secteurService.getSecteur(id);
        if (secteur.get().getStatus() == 0) {
            secteur.get().setStatus(1);
        } else {
            secteur.get().setStatus(0);
        }
        secteurService.saveSecteur(secteur.get());
        return "redirect:/secteurs";
    }
}
