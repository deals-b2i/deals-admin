package com.nickdev.dealsadmin.controller;

import com.nickdev.dealsadmin.model.Annonce;
import com.nickdev.dealsadmin.model.Secteur;
import com.nickdev.dealsadmin.service.AnnonceService;
import com.nickdev.dealsadmin.service.SecteurService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Optional;

@Data
@Controller
public class AnnonceController {

    @Autowired
    private AnnonceService annonceService;

    @Autowired
    private SecteurService secteurService;

    @GetMapping("/annonces")
    public String index(Model model) {
        Iterable<Annonce> annonces = annonceService.getAnnonces();
        model.addAttribute("annonces", annonces);

        return "annonces/index";
    }

    @GetMapping("/annonces/create")
    public String create(Model model) {
        Annonce annonce = new Annonce();
        Iterable<Secteur> secteurs = secteurService.getSecteurs();
        model.addAttribute("annonce", annonce);
        model.addAttribute("secteurs", secteurs);

        return "annonces/create";
    }

    @PostMapping("/annonces/store")
    public String store(@ModelAttribute("annonce") Annonce secteur) {
        annonceService.saveAnnonce(secteur);

        return "redirect:/annonces";
    }

    @GetMapping("/annonces/edit/{id}")
    public ModelAndView edit(@PathVariable(name = "id") Long id) {
        ModelAndView mav = new ModelAndView("annonces/edit");
        Optional<Annonce> annonce = annonceService.getAnnonce(id);
        mav.addObject("annonce", annonce);

        return mav;
    }

    @GetMapping("/annonces/delete/{id}")
    public String delete(@PathVariable(name = "id") Long id) {
        annonceService.deleteAnnonce(id);
        return "redirect:/annonces";
    }

    @GetMapping("/annonces/update/status/{id}")
    public String updateStatus(@PathVariable(name = "id") Long id) {
        Optional<Annonce> annonce = annonceService.getAnnonce(id);
        if (annonce.get().getStatus() == 0) {
            annonce.get().setStatus(1);
        } else {
            annonce.get().setStatus(0);
        }
        annonceService.saveAnnonce(annonce.get());
        return "redirect:/annonces";
    }

}
