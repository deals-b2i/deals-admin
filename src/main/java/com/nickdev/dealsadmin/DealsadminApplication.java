package com.nickdev.dealsadmin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DealsadminApplication {

    public static void main(String[] args) {
        SpringApplication.run(DealsadminApplication.class, args);
    }

}
